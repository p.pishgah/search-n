import { combineReducers } from 'redux'
import search from "./search/searchReducer";

const reducers = {
    search,
}

export default combineReducers(reducers)