import * as constant from './searchConstant';

const initialState = {
    error: false,
    page: 1,
    data: [],
    searchTerm: "",
    key: "AIzaSyCoW9mEWadzbMUGo7AZZ8wN2lJm7KoXx1c"
}
const search = ( state = initialState, action ) => {
    switch (action.type) {
        case constant.SAVE_DATA:
            return {
                ...state,
                data: action.payload,
                error: false,
            };
        case constant.CLEAR_DATA:
            return {
                ...state,
                data: [],
                error: false
            };
        case constant.SEARCH_TERM:
            return {
                ...state,
                searchTerm: action.payload
            };
        case constant.CHANGE_KEY:
            return {
                ...state,
                key: action.payload,
            };
        case constant.ERROR:
            return {
                ...state,
                error: action.payload
            };
        default:
            return state
    }
}

export default search;