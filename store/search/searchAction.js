import * as constant from './searchConstant';
import axios from 'axios';
import { cx, apiUrl } from '../../public/config';

export const doSearch = (searchTerm, page) => async (dispatch, getState) => {
    if (searchTerm === ""){
        dispatch({type: constant.CLEAR_DATA})
    } else {
        try {
            let res = await axios.get(`${apiUrl}?key=${getState().search.key}&` +
                `cx=${cx}&q="${searchTerm}"&start=${(page * 10) - 9}`);
            if(res.status === 200) {
                dispatch({
                    type: constant.SAVE_DATA,
                    payload: res.data,
                })
            }
        } catch (res) {
            if (res.message === "Request failed with status code 429"){
                dispatch({
                    type: constant.ERROR,
                    payload: 429,
                })
            } else
                {
                dispatch({
                    type: constant.ERROR,
                    payload: 440,
                })
            }
        }
    }
};

export const changeKey = (key) => ({
    type:constant.CHANGE_KEY,
    payload: key
});

export const searchTerm = (searchTerms) => ({
    type: constant.SEARCH_TERM,
    payload: searchTerms
});