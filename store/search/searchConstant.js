export const SAVE_DATA = 'SAVE_DATA';
export const SEARCH_TERM = 'SEARCH_TERM';
export const QUERIES_LIMIT = 'QUERIES_LIMIT';
export const ERROR = 'ERROR';
export const CHANGE_KEY = 'CHANGE_KEY';
export const CLEAR_DATA = 'CLEAR_DATA';