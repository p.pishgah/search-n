import React from 'react';
import s from './resultShowText.module.css';
import { Alert } from 'antd';

const ResultShowText = () => {
    return(
        <div className={s.holder}>
            <Alert
               message="your search results would be shown here."
               type="info" />
        </div>
    )
}

export default ResultShowText;

