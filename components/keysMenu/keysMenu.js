import React from 'react';
import { useDispatch } from "react-redux";
import { Menu, Dropdown } from 'antd';
import { DownOutlined } from '@ant-design/icons';
import { useSelector} from "react-redux";
import s from './keysMenu.module.css';
import { keys } from '../../public/config';
import { changeKey } from "../../store/search/searchAction";

const KeysMenu = () => {
    const dispatch = useDispatch();
    let currentKey = useSelector(state => state.search.key);

    const menu = (
        <Menu>
            {
                keys.filter(key => key !== currentKey).map((key, index) =>
                    <Menu.Item onClick={() => dispatch(changeKey(key))} key={key}>
                        <div className={s.keyName}>key number
                            <span className={s.number}>{index + 1}</span>
                        </div>
                    </Menu.Item>
                )
            }
        </Menu>
    );

    return(
        <div>
            <Dropdown overlay={menu}>
                <a className={s.title} >
                    select another search key <DownOutlined className={s.icon} />
                </a>
            </Dropdown>
        </div>
    )
}

export default KeysMenu;