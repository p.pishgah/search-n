import React from 'react';
import { Spin } from 'antd';
import s from './loading.module.css';

const Loading = () => {
    return(
        <div className={s.spinHolder}>
            <Spin
            size="large"
            tip="loading..."
            />
        </div>
    )
}

export default Loading;