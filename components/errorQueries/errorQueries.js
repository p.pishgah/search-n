import React from 'react';
import { Alert } from 'antd';
import s from './errorQueries.module.css';

const ErrorQueries = () => {
    return(
        <div className={s.holder}>
            <Alert
                message={
                    <span>
                        <p className={s.errorText}>Limit of 'Queries per day' finished with this key.</p>
                        <p >Please<ins className={s.guide}> select another search key </ins> from top-left.</p>
                    </span>
                    }
                type="error"
                showIcon
            />
        </div>
    )
}

export default ErrorQueries;


