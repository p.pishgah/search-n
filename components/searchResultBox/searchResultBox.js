import React from 'react';
import s from './searchResultBox.module.css';

const SearchResultBox = ({title, link ,index, page}) => {
    const rowNumber = (page * 10) + index + 1 - 10
    return(
        <a className={s.mainBox} href={link} target="_blank">
            <span className={s.number}>{rowNumber}</span>
            <h3 className={s.title}>{title}</h3>
        </a>
    )
}

export default SearchResultBox;