import React from 'react';
import s from './noData.module.css';
import { Alert } from 'antd';
import { useSelector } from "react-redux";

const NoData = () => {
    const searchTerm = useSelector( state => state.search.searchTerm)
    const errorText = <p>there was No result for <b>{searchTerm}</b></p>
    return(
        <div className={s.holder}>
            <Alert
                message={errorText}
                type="warning"
                showIcon
            />
        </div>
    )
}

export default NoData;