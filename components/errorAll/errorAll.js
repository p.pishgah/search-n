import React from 'react';
import { Alert } from 'antd';
import s from './errorAll.module.css';

const ErrorAll = () => {
    return(
        <div className={s.holder}>
            <Alert
                message="The application failed to run. Please refresh the page."
                type="error"
                showIcon
            />
        </div>
    )
}

export default ErrorAll;