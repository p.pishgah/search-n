import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import Head from 'next/head';
import {Input, Button, Alert, Modal} from 'antd';
import {DoubleLeftOutlined, DoubleRightOutlined, SearchOutlined} from '@ant-design/icons';
import { debounce } from "debounce";
import Loading from '../components/loading/loading';
import s from '../styles/Home.module.css';
import SearchResultBox from '../components/searchResultBox/searchResultBox';
import ResultShowText from '../components/resultShowText/resultShowText';
import NoData from '../components/noData/noData';
import KeysMenu from "../components/keysMenu/keysMenu";
import ErrorAll from '../components/errorAll/errorAll';
import ErrorQueries from "../components/errorQueries/errorQueries";
import { doSearch, searchTerm } from '../store/search/searchAction';

const Home = () => {
    const dispatch = useDispatch();
    const search = useSelector(state => state.search.data);
    const error = useSelector(state => state.search.error);
    const [ loading, setLoading ] = useState(false);
    const [ page, setPage ] = useState(1);
    const [ searchTerms, setSearchTerms ] = useState('');

    const onChange = async (e) => {
        setPage(1);
        setLoading(true);
        setSearchTerms(e.target.value);
        dispatch(searchTerm(searchTerms));
        await dispatch(doSearch(e.target.value, page));
        setLoading(false);
    };

    const debounceChange = debounce(onChange, 200);

    const pageNext = async (currentPage) => {
        setLoading(true);
        setPage(currentPage + 1);
        await dispatch(doSearch(searchTerms, currentPage + 1));
        setLoading(false);
    };
    const pagePrevious = async (currentPage) => {
        setLoading(true);
        setPage(currentPage - 1);
        await dispatch(doSearch(searchTerms, currentPage - 1));
        setLoading(false);
    };

    return (
        <>
            <Head>
                <title>Custom Search API</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <header className={s.header}>
                <h2 className={s.title}>Search In Google Custom Search API</h2>
                <div className={s.keysMenuHolder}>
                    <KeysMenu />
                </div>
            </header>
            <main className={s.main}>
                { error === 440 && <ErrorAll /> }
                { error === 429  && <ErrorQueries /> }
                <Input
                    placeholder="input search text"
                    allowClear
                    onChange={debounceChange}
                    prefix={<SearchOutlined className={s.searchIcon} />}
                    className={s.searchInput}
                />
                { loading && <Loading /> }
                { searchTerms === "" && <ResultShowText /> }
                <div className={s.resultsHolder}>
                    {!error && !loading && search?.items && search.items.map((i, index) =>
                        <SearchResultBox
                            key={index}
                            index={index}
                            page={page}
                            title={i.title}
                            link={i.link}
                        />
                    )}
                    {
                        search?.items &&
                        <div className={s.paginationHolder}>
                            <Button onClick={() => pagePrevious(page) } className={s.previousPage} disabled={error || !search?.queries?.previousPage}><DoubleLeftOutlined />Previous Page</Button>
                            <Button onClick={() => pageNext(page)} className={s.nextPage} disabled={error || !search?.queries?.nextPage}>Next Page<DoubleRightOutlined /></Button>
                        </div>
                    }
                    {
                        searchTerms !== "" && search?.items === undefined &&
                        !loading && !error && <NoData />
                    }
                </div>
            </main>
            <footer className={s.footer}>
                <h3 className={s.footerTitle}>Search In Google Custom Search API</h3>
            </footer>
        </>
    )
}

export default Home;